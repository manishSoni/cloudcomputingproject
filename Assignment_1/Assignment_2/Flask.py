#!flask/bin/python
from flask import Flask, jsonify, request
from flask import Response
from flask import make_response
import hashlib
import sqlite3


app = Flask(__name__)


def checkUserNameInDb(username):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from User WHERE username = ?",(username,))
    userData = cursor.fetchall()
    connectionState.close()
    if len(userData) == 0:
        return 1
    return 0

def getUserDataFromDb(userDataList):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from User")
    userData = cursor.fetchall()
    userDictionary = {}
    for i in userData:
        userDictionary = {}
        userDictionary["username"] = i[0]
        userDictionary["password"] = i[1]
        userDataList.append(userDictionary)
    connectionState.close()

def deleteUserFromDb(username):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("DELETE from User WHERE username = ?",(username,))
    connectionState.commit()
    connectionState.close()

def addUserDataToDb(userDataInJsonFormat):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO User (username, password) VALUES (?, ?)",(userDataInJsonFormat['username'],userDataInJsonFormat['password']))
    connectionState.commit()
    connectionState.close()    

def getCategoryFromDb(categoryList):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from Category")
    categoryData = cursor.fetchall()
    categoryDictionary = {}
    for i in categoryData:
        categoryDictionary = {}
        categoryDictionary[i[0]] = i[1]
        categoryList.append(categoryDictionary)
    connectionState.commit()
    connectionState.close()

def addCategoryToDb(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO Category (categoryname, numberofacts) VALUES (?, ?)",(categoryname,0))
    connectionState.commit()
    connectionState.close() 


def checkCategory(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT categoryname from Category WHERE categoryname = ?",(categoryname,))
    categoryCheck = cursor.fetchall() 
    connectionState.commit()
    connectionState.close()
    if len(categoryCheck):
        return 1
    return 0   

def deleteCategoryFromDb(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("DELETE from Category WHERE categoryname = ?",(categoryname,))
    connectionState.commit()
    connectionState.close()


@app.route('/api/v1/users', methods = ['POST','DELETE','GET','PUT'])
def addUser():
    if request.method == 'POST': 
        userDataInJsonFormat = request.get_json()
        print(checkUserNameInDb(userDataInJsonFormat['username']))
        if checkUserNameInDb(userDataInJsonFormat['username']) :
            userDataList = []
            addUserDataToDb(userDataInJsonFormat)
            getUserDataFromDb(userDataList)
            return jsonify({'users': userDataList}), 201
        else:
            return jsonify({}), 400
    else:
        return jsonify({}),405

@app.route('/api/v1/users/<username>', methods = ['POST','DELETE','GET','PUT'])
def deleteUser(username):
    if request.method == "DELETE":
        if checkUserNameInDb(username):
            deleteUserFromDb(username)
            getUserDataFromDb(userDataList)
            return jsonify({'users': userDataList}), 200
        else:
            return jsonify({}),400    
    else:
        return jsonify({}),405        


@app.route('/api/v1/categories', methods = ['POST','DELETE','GET','PUT'])
def categories():
    if isinstance(request.get_json(),list):
        if request.method == 'POST': 
                if len(request.get_json()):
                    addCategoryToDb(request.get_json()[0])
                    categoryList = []
                    getCategoryFromDb(categoryList)
                    return jsonify({'categories': categoryList}), 201
                else:
                    return jsonify({}),400        
        else:
            return jsonify({}),405
    else:
        if request.method == 'GET': 
                categoryList = []
                getCategoryFromDb(categoryList)
                if len(categoryList):
                    return jsonify({'categories': categoryList}), 201
                else:
                    return jsonify({}),204        
        else:
            return jsonify({}),405


@app.route('/api/v1/categories/<categoryname>', methods = ['POST','DELETE','GET','PUT'])
def deleteCategories(categoryname):
    print(categoryname)
    if request.method == 'DELETE': 
            categoryList = []
            if checkCategory(categoryname):
                deleteCategoryFromDb(categoryname)
                getCategoryFromDb(categoryList)
                return jsonify({'categories': categoryList}), 200
            else:
                return jsonify({}),400        
    else:
        return jsonify({}),405




if __name__ == '__main__':
    app.run(debug=True)