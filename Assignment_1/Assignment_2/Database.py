import sqlite3
connectionState = sqlite3.connect("SelfieLessActs.db")
cursor = connectionState.cursor()


def createTables():
	cursor.execute("""CREATE TABLE if not exists User (
										username TEXT PRIMARY KEY,
										password TEXT) """)
	cursor.execute("""CREATE TABLE if not exists Category (
									categoryname TEXT PRIMARY KEY,
									numberofacts TEXT)""")
	cursor.execute("""CREATE TABLE if not exists Acts (
										categoryname TEXT, 
										actId INT PRIMARY KEY,
										username TEXT,
										posttime TEXT,
										captiontext TEXT,
										img TEXT,
										upvotes INT,
										FOREIGN KEY(categoryname) REFERENCES Category(categoryname))""")
	connectionState.commit()
# 	# connectionState.row_factory = sql.Row
# 	# cursor = connectionState.cursor()
# 	# cursor.execute("SELECT * from User")


createTables()