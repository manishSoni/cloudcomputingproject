import sqlite3

# Function which checks whether a given user is present in the database or no
def checkUserNameInDb(username):
    connectionState = sqlite3.connect("UserData.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from User WHERE username = ?",(username,))
    userData = cursor.fetchall()
    connectionState.commit()
    connectionState.close()
    if userData == None:
        return 1
    if len(userData) == 0:
        return 1
    return 0

# Function which returns the user name from the db
def getUserNameFromDb():
    connectionState = sqlite3.connect("UserData.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT username from User")
    userData = cursor.fetchall()
    userNameList = []
    if userData != None and len(userData) > 0: 
        for i in userData:
             userNameList.append(i[0])
    connectionState.commit()
    connectionState.close()
    return userNameList

# Function which delets a user from database
def deleteUserFromDb(username):
    connectionState = sqlite3.connect("UserData.db")
    cursor = connectionState.cursor()
    cursor.execute("DELETE from User WHERE username = ?",(username,))
    connectionState.commit()
    connectionState.close()

# Function for adding a user to database
def addUserDataToDb(userDataInJsonFormat):
    connectionState = sqlite3.connect("UserData.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO User (username, password) VALUES (?, ?)",(userDataInJsonFormat['username'],userDataInJsonFormat['password']))
    connectionState.commit()
    connectionState.close()    

