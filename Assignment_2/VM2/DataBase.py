import sqlite3
from operator import itemgetter
import binascii
import base64
import ServiceLayer as serviceLayer

# Function which checks whether a given user is present in the database or no
def checkUserNameInDb(username):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from User WHERE username = ?",(username,))
    userData = cursor.fetchall()
    connectionState.commit()
    connectionState.close()
    if userData == None:
        return 1
    if len(userData) == 0:
        return 1
    return 0

# Function for returing acts in a range
def getActsInGivenRange(categoryname, startRange, endRange):
    actsList = getActsForCategoryFromDb(categoryname)
    if len(actsList) < endRange:
        return actsList
    actsInRange = []
    for i in range(startRange-1, endRange-1):
        actsInRange.append(actsList[i])
    return actsInRange

# Function which returns list of users in list of dictionary format
def getUserDataFromDb(userDataList):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from User")
    userData = cursor.fetchall()
    userDictionary = {}
    if userData != None and len(userData) > 0: 
	    for i in userData:
	        userDictionary = {}
	        userDictionary["username"] = i[0]
	        userDictionary["password"] = i[1]
	        userDataList.append(userDictionary) 
    connectionState.commit()
    connectionState.close()

# Function which delets a user from database
def deleteUserFromDb(username):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("DELETE from User WHERE username = ?",(username,))
    connectionState.commit()
    connectionState.close()

# Function for adding a user to database
def addUserDataToDb(userDataInJsonFormat):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO User (username, password) VALUES (?, ?)",(userDataInJsonFormat['username'],userDataInJsonFormat['password']))
    connectionState.commit()
    connectionState.close()    

# Function which returns all the category from db
def getCategoryFromDb(categoryDictionary):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from Category")
    categoryData = cursor.fetchall()
    print(categoryData)
    if categoryData != None or len(categoryData) > 0:
    	for i in categoryData:
        	categoryDictionary[i[0]] = i[1]
    connectionState.commit()
    connectionState.close()
    # print(categoryDictionary)

# Function for adding a category to db
def addCategoryToDb(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO Category (categoryname, numberofacts) VALUES (?, ?)",(categoryname,0))
    connectionState.commit()
    connectionState.close() 

# Function for checking category name in Db
def checkcategorynameInDb(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT categoryname from Category WHERE categoryname = ?",(categoryname,))
    categoryCheck = cursor.fetchall() 
    connectionState.commit()
    connectionState.close()
    if categoryCheck == None:
        return 0
    if categoryCheck != None and len(categoryCheck) == 0 :
        return 0
    return 1   

# Function which returns the list of acts for a given category
def getActsForCategoryFromDb(categoryname):
    actsList = []
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT * from Acts WHERE categoryname = ?",(categoryname,))
    actsData = cursor.fetchall() 
    connectionState.commit()
    connectionState.close()
    actsInCategory = {}
    if actsData != None and len(actsData) > 0:
	    for i in actsData:
	        actsInCategory = {}
	        actsInCategory['categoryname'] = i[0] 
	        actsInCategory['actId'] = i[1]
	        actsInCategory['username'] = i[2]
	        actsInCategory['timestamp'] = i[3]
	        actsInCategory['caption'] = i[4]
	        actsInCategory['upvotes'] = i[6]
	        actsInCategory['img64'] = i[5]
	        actsList.append(actsInCategory)
	    actsList = sorted(actsList, key=itemgetter('timestamp'),reverse = True) 
    return actsList

# Function which deletes a category from db
def deleteCategoryFromDb(categoryname):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("DELETE from Category WHERE categoryname = ?",(categoryname,))
    connectionState.commit()
    connectionState.close()

# Function which returns number of acts in a given category
def getActsSizeForCategoryFromDb(categoryname):
    actsSize = []
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute('SELECT numberofacts FROM Category WHERE categoryname = ?',(categoryname,))
    numberOfActs = cursor.fetchone()
    connectionState.commit()
    connectionState.close()
    if  numberOfActs != None and len(numberOfActs) > 0:
        return [numberOfActs[0]]
    return 0

# Function for checking if the given act is present in db
def checkActInDb(actId):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute('SELECT * FROM Acts WHERE actId = ?',(actId,))
    act = cursor.fetchall()
    connectionState.commit()
    connectionState.close()
    if act!= None and len(act) == 1:
        return 1
    return 0

# Function for updating the upvote count
def updateUpvoteForActInDb(actId):
    print(actId)
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute('UPDATE Acts SET upvotes = upvotes+1 WHERE actId = ?',(actId,))
    connectionState.commit()
    actData = cursor.fetchall()
    connectionState.close()

# Function for deleting an act from db
def deleteActFromDb(actId):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("SELECT categoryname from Acts WHERE actId = ?",(actId,))
    categoryname = cursor.fetchone()[0]
    cursor.execute("DELETE from Acts WHERE actId = ?",(actId,))
    cursor.execute('UPDATE Category SET numberofacts = numberofacts - 1 WHERE categoryname = ?',(categoryname,))
    connectionState.commit()
    connectionState.close()

# Function for adding an act to db
def addActToDb(actInJsonFormat):
    connectionState = sqlite3.connect("SelfieLessActs.db")
    cursor = connectionState.cursor()
    cursor.execute("INSERT INTO Acts (categoryname, actId, username, posttime, captiontext, img, upvotes) VALUES (?, ?, ?, ?, ?, ?, ?)",
        (actInJsonFormat["categoryName"], actInJsonFormat["actId"], actInJsonFormat["username"], actInJsonFormat["timestamp"],
            actInJsonFormat["caption"], actInJsonFormat["imgB64"], 0))
    cursor.execute("UPDATE Category SET numberofacts = numberofacts + 1 WHERE categoryname = ?",(actInJsonFormat["categoryName"],))
    connectionState.commit()
    connectionState.close()     

