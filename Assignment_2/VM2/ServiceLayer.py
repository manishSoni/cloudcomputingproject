import datetime
import time
import base64
import binascii

# Function for validating password if the given password is in sha1 format or no
def validatePassword(password):
    if len(password) != 40:
        return False
    try:
        shaInt = int(password, 16)
    except ValueError:
        return False
    return True

# Time format : “DD-MM-YYYY:SS-MM-HH”,
def validateTimeFormat(timestamp):
    timestamp = timestamp.split(":")
    inputDate = timestamp[0].split("-")
    inputTime = timestamp[1].replace("-",":")
    try : 
        datetime.datetime(int(inputDate[2]), int(inputDate[1]), int(inputDate[0])) 
        try:
            time.strptime(inputTime,'%S:%M:%H')
        except:
            return 0
    except:
        return 0
    return 1

# Function for checking if the given image string is in base64 part or no
def validateImageFormat(imageString):
    try:
        base64.decodestring(imageString.encode())
        return 1
    except binascii.Error:
        return 0

def validateUserFormat(userDataInJsonFormat):
    try:
        userDataInJsonFormat["username"]
        try:
            userDataInJsonFormat["password"]
        except:
            return 0
    except:
        return 0
    return 1


# Function for checking if the upload act arguments are valid or np
def validateActInputFormat(actInJsonFormat):
    try:
        actInJsonFormat["actId"]
    except:
        return 0
    try:
        actInJsonFormat["username"]
    except:
        return 0
    try:
        actInJsonFormat["categoryName"]
    except:
        return 0
    try:
        actInJsonFormat["caption"]
    except:
        return 0
    try:
        actInJsonFormat["timestamp"]
    except:
        return 0
    try:
        actInJsonFormat["imgB64"]
    except:
        return 0
    return 1
