 #!flask/bin/python
from flask import Flask, jsonify, request, abort, render_template
from flask import Response
from flask import make_response
import sqlite3
import time
import requests
import hashlib
import base64
import binascii
import time
from flask_cors import CORS

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
CORS(app, resources={r"/*": {"origins": "*"}})
app.config["CORS_SUPPORTS_CREDENTIALS"]=True


@app.route("/homepage")
def homepage():
    return render_template('index.html')

@app.route("/aboutus")
def aboutus():
	return render_template('Aboutus.html')

@app.route("/Signup")
def Signup():
    return render_template('Signup.html')

@app.route("/DeleteUser")
def DeleteUser():
    return render_template('Delete.html')

@app.route("/categoryFile")
def categoryFile():
    return render_template('CategoryPage.html')

@app.route("/ActsPage")
def ActsPage():
    return render_template('ActsPage.html')

@app.route("/AddCategory")
def AddCategory():
    return render_template('AddCategory.html')

@app.route("/RemoveCategory")
def RemoveCategory():
    categories = []
    response = requests.get("http://localhost:5000/getCategoryListCallFromClient")
    try:
        categories = response.json()["categories"]
    except:
        categories = []
    return render_template('Remove.html', rows = categories)

@app.route("/UploadAnAct")
def UploadAnAct():
    return render_template('UploadAnAct.html')

@app.route("/actsInCategoryRequest")
def actsInCategoryRequest():
    categories = []
    response = requests.get("http://localhost:5000/getCategoryListCallFromClient")
    try:
        categories = response.json()["categories"]
        print(categories)
    except:
        categories = []
    return render_template('GetActForCategory.html', rows = categories)

@app.route("/actsInCategoryRange")
def actsInCategoryRange():
    actsForCategory = []
    response = requests.get("http://localhost:5000/api/v1/categories", json = {})
    try:
        responseJsonObject = response.json()
        actsForCategory = responseJsonObject
    except:
        actsForCategory = []
    return render_template('ActInRange.html', rows = actsForCategory)

@app.route("/actsInCategoryRangeRequest", methods = ["POST"])
def actsInCategoryRangeRequest():
    categoryname = request.form["categoryname"]
    start = request.form["start"]
    end = request.form["end"]
    response = requests.get("http://localhost:5000/api/v1/categories/" + categoryname + "/acts?start=" + start + "&end=" + end, json = {})
    try:
        responseJsonObject = response.json()

        actsObject = responseJsonObject
        actsObjectAsActIdkey = {}
        for i in actsObject:
            actsObjectAsActIdkey[i["actId"]] = []
            actsObjectAsActIdkey[i["actId"]].extend([i["categoryname"], i["username"], i["timestamp"], i["caption"], i["upvotes"],i["img64"]])
        return render_template('Category.html', rows = actsObjectAsActIdkey)
    except:
        return render_template('Category.html', rows = {})

@app.route("/actsInGivenCategory",methods = ["POST"])
def actsInGivenCategory():
    response = requests.get("http://localhost:5000/api/v1/categories/" + request.form["categoryname"] +"/acts")
    try:
        responseJsonObject = response.json()
        actsObject = responseJsonObject
        actsId = []
        actsObjectAsActIdkey = {}
        for i in actsObject:
            actsObjectAsActIdkey[i["actId"]] = []
            actsObjectAsActIdkey[i["actId"]].extend([i["categoryname"], i["username"], i["timestamp"], i["caption"], i["upvotes"],i["img64"]])
        return render_template('Category.html', rows = actsObjectAsActIdkey)
    except:
        return render_template('Category.html', rows = {})

@app.route("/actForCategories")
def actForCategories():
    actsForCategory = []
    response = requests.get("http://localhost:5000/api/v1/categories", json = {})
    try:
        responseJsonObject = response.json()
        actsForCategory = responseJsonObject
    except:
        actsForCategory = []
    print(actsForCategory)
    return render_template('GetActSize.html', rows = actsForCategory)

@app.route("/actSizeForAGivenCategory")
def actSizeForAGivenCategory():
    categories = []
    response = requests.get("http://localhost:5000/getCategoryListCallFromClient")
    try:
        categories = response.json()["categories"]
        print(categories)
    except:
        categories = []
    return render_template("ActSize.html", rows = categories)

@app.route("/getShaPassword",methods = ["POST"])
def getShaPassword():
    if request.method == 'POST':
        inputPassword = request.get_json()[0]
        hashedPassword = hashlib.sha1(inputPassword.encode()).hexdigest()
        print(hashedPassword)
        return jsonify({"hashedPassword":hashedPassword}) ,200
        
@app.route("/UploadAnActToBackend",methods = ["POST"])
def UploadAnActToBackend():
    if request.method == 'POST':
        actObject = request.get_json()
        timeStamp = time.strftime('%d/%m/%Y %S:%M:%H')
        timeStamp = timeStamp.replace(":","-")
        timeStamp = timeStamp.replace(" ",":")
        timeStamp = timeStamp.replace("/","-")
        actObject["timestamp"] = timeStamp
        response = requests.get("http://localhost:5000/api/v1/categories/" + actObject["categoryName"] + "/acts?start=" + str(1) + "&end=" + str(2))
        actId = []
        try:
            # retrievedActId = response.json()["acts"][0]["actId"]
            retrievedActId = response.json()
            for i in retrievedActId:
                    actObject["actId"] = i["actId"] + 1
        except:
            actObject["actId"] = 1
        requests.post("http://localhost:5000/api/v1/acts", json = actObject)
        return jsonify({}) ,200


if __name__ == '__main__':
    app.run(host = '0.0.0.0',port = '5001',debug=True)
