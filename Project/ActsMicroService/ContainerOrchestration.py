from flask import Flask, jsonify, request
from flask_cors import CORS
import os
import time
import requests
import docker
import schedule
client = docker.from_env()

port = 8000
active_container = []
active_port_number = []
request_counter = 0
timer_start = False
initial_container = False
first_request_made = False
timer = 0



app = Flask(__name__)

def create_container():
	global client
	global port
	container = (client.containers.create("acts:latest", ports = {"80/tcp": port}))
	container.attrs["NetworkSettings"]["Ports"] = port
	container.start()
	port += 1
	return container

def increment_request_counter():
	global request_counter
	request_counter += 1

def scale_up_container(end_value):
	for i in range(0, end_value - len(active_container)):
		container = create_container()
		active_container.append(container)

def scale_down_container(end_value):
	while len(active_container) != end_value:
		container = active_container.pop()
		container.stop()

def check_request_counter():
	global request_counter
	if request_counter <= 2:
		if len(active_container) > 1:
			scale_down_container(1)
	elif request_counter > 2 and request_counter <= 4:
		if len(active_container) > 2:
			scale_down_container(2)
		scale_up_container(2)
	elif request_counter > 4  and request_counter <= 60:
		if len(active_container) > 3:
			scale_down_container(3)
		scale_up_container(3)
	elif request_counter > 60  and request_counter <= 80:
		if len(active_container) > 4:
			scale_down_container(4)
		scale_up_container(4)
	elif request_counter > 80  and request_counter <= 100:
		if len(active_container) > 5:
			scale_down_container(5)
		scale_up_container(5)
	elif request_counter > 100 and request_counter <= 120:
		if len(active_container) > 6:
			scale_down_container(6)
		scale_up_container(6)
	elif request_counter > 120 and request_counter <= 140:
		if len(active_container) > 7:
			scale_down_container(7)
		scale_up_container(7)
	elif request_counter > 140 and request_counter <= 160:
		if len(active_container) > 8:
			scale_down_container(8)
		scale_up_container(8)
	elif request_counter > 180 and request_counter <= 180:
		if len(active_container) > 9:
			scale_down_container(9)
		scale_up_container(9)
	else:
		if len(active_container) > 10:
			scale_down_container(10)
		scale_up_container(10)

@app.route("/initialize_container", methods = ["POST"])
def initialize_container():
	global initial_container
	global port
	try:
		container = client.containers.create("acts:latest", ports = {"80/tcp": port})
		container.attrs["NetworkSettings"]["Ports"] = port
		container.start()
		active_container.append(container)
		active_port_number.append(port)
		initial_container = True
		port += 1
		return jsonify({}), 200
	except:
		return jsonify({}), 400

def initialize_timer(timer):
	global timer_start
	if timer_start == False:
		timer_start = True
		timer += 1
	return timer

def increment_timer(timer):
	timer += 1
	time.sleep(1)
	return timer

def set_parameter_to_zero():
	global timer_start
	global timer
	global request_counter
	timer_start = False
	timer = 0
	request_counter = 0


def forward_request(path, request_counter):
	server_path = "http://127.0.0.1:" + str(active_container[request_counter%len(active_container)].attrs["NetworkSettings"]["Ports"]) + "/" + path
	print(server_path)
	time.sleep(1)
	response = requests.get(server_path)
	request_counter += 1
	return response.json(), response.status_code




@app.route("/health_check")
def health_check():
	api_routes = [ "/api/v1/categories/<categoryname>", "/api/v1/categories/<categoryname>/acts", "/api/v1/categories/<categoryname>/acts/size",
	"/api/v1/acts/upvote", "/api/v1/acts/counts", "/api/v1/acts/", "/api/v1/categories", "/api/v1/_count" ]
	global timer_start
	global timer
	if timer_start == False and first_request_made == True:
			timer = initialize_timer(timer)
	elif first_request_made == True:
			timer = increment_timer(timer)
	if timer == 5:
		check_request_counter()
		set_parameter_to_zero()
	print("In health check function", len(active_container))
	for container in active_container:
		time.sleep(1)
		server_path = "http://127.0.0.1:" + str(container.attrs["NetworkSettings"]["Ports"]) + "/api/v1/_health"
		response = requests.get(server_path)
		print(response.status_code)
		if response.status_code == 500:
			port_number = container.attrs["NetworkSettings"]["Ports"]
			active_container.remove(container)
			container.stop()
			new_container = (client.containers.create("acts:latest", ports = {"80/tcp": port_number}))
			new_container.attrs["NetworkSettings"]["Ports"] = port_number
			new_container.start()
			active_container.append(new_container)
	return jsonify("All okay"), 200

@app.route("/<path:path>")
def get_all_url(path):
	# global initial_container
	# if initial_container == False:
	# 	initialize_container()
	if path == "/":
		pass
	else:
		global timer_start
		global timer
		global request_counter
		global first_request_made
		first_request_made = True
		if timer_start == False:
			timer = initialize_timer(timer)
		else:
			timer = increment_timer(timer)
		if timer == 5:
			check_request_counter()	
			set_parameter_to_zero()
		if path in api_routes:
			increment_request_counter()
		respone_json_object, response_status_code = forward_request(path, request_counter)
		return jsonify(respone_json_object), response_status_code



if __name__ == '__main__':
	app.run(host = '0.0.0.0', port = 80, debug = True)
	