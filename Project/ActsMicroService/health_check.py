import schedule
import requests
import time


def job():
	time.sleep(1)
	response = requests.get("http://127.0.0.1:80/health_check")
	print(response.status_code)

def start_orcehstration():
	print("In orchestration function")
	response = requests.post("http://127.0.0.1:80/initialize_container")
	if response.status_code == 200:
		schedule.every().second.do(job)
		while True:
		    schedule.run_pending()
		    time.sleep(1)


if __name__ == '__main__':
	start_orcehstration()
