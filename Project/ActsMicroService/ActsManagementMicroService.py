 #!flask/bin/python
from flask import Flask, jsonify, request
import hashlib
import time
import requests
import DataBase as db
import ServiceLayer as service
from flask_cors import CORS


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.config['JSON_SORT_KEYS'] = False
app.config["CORS_SUPPORTS_CREDENTIALS"]=True
requestsCount = 0
crashCount = 0

# Function for incrementing the HTTP request counter
def incrementHTTPRequestCount():
    global requestsCount
    requestsCount += 1

# Function for getting the HTTP request counter
def getHTTPRequestCount():
    global requestsCount
    return requestsCount    

# Function for getting the server status
def checkServerCrashStatus():
    global crashCount
    if crashCount == 1:
        return 1
    return 0

# Function for resetting the HTTP request made
def resetHTTPRequestCounter():
    global requestsCount
    requestsCount = 0

# Function for getting the category used by the client side
@app.route("/getCategoryListCallFromClient", methods = ["GET"])
def getCategoryListCallFromClient():
    if checkServerCrashStatus():
        return jsonify(None), 500
    categoryDictionary = {}
    db.getCategoryFromDb(categoryDictionary)
    categories = []
    for key, value in categoryDictionary.items():
        categories.append({key:value})
    return jsonify({'categories': categories}), 200
        
# API 3 and 4 : List all categories and Add a category
@app.route('/api/v1/categories', methods = ['POST','DELETE','GET','PUT'])
def categories():
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    inputCategory = []
    try:
        inputCategory = (request.get_json())
    except:
        inputCategory = []
    if inputCategory != None and len(inputCategory):
        if request.method == 'POST': 
                if len(inputCategory):
                    if db.checkcategorynameInDb(inputCategory[0]) == 0:
                        db.addCategoryToDb(inputCategory[0])
                        categoryList = {}
                        db.getCategoryFromDb(categoryList)
                        return jsonify({}), 201
                    else:
                        return jsonify({}), 400
                else:
                    return jsonify({}),400        
        else:
            return jsonify({}),405
    else:
        if request.method == 'GET': 
            categoryDictionary = {}
            db.getCategoryFromDb(categoryDictionary)
            if len(categoryDictionary):
                return jsonify(categoryDictionary), 200
            else:
                return jsonify({}),204        
        else:
            return jsonify({}),405
        return jsonify({}), 204 
            
# API 5 : Remove a category
@app.route('/api/v1/categories/<categoryname>', methods = ['POST','DELETE','GET','PUT'])
def deleteCategories(categoryname):
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == 'DELETE':
            if db.checkcategorynameInDb(categoryname):
                db.deleteCategoryFromDb(categoryname)
                return jsonify({}), 200
            else:
                return jsonify({}),400        
    else:
        return jsonify({}),405
    
# API 6 and 7 : Get acts in a given range and get all the acts of a category    
@app.route('/api/v1/categories/<categoryname>/acts',methods=['GET','PUT','POST','DELETE'])
def getActsInCategory(categoryname):
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    start = request.args.get('start')
    end = request.args.get('end')
    if (type(start) == int or type(end) == int) or (type(start) == str or type(end) == str):
        if (type(start) == str or type(end) == str):
            start = int(start)
            end = int(end)
        if start < 0 or end < 0 or start > end:
            return jsonify({}), 204
        if db.checkcategorynameInDb(categoryname):
            actsSize = db.getActsSizeForCategoryFromDb(categoryname)
            if start > len(actsSize):
                return jsonify({}), 400
        if request.method == 'GET':
            actsInRange = db.getActsInGivenRange(categoryname, start, end)
            if len(actsInRange):
                if len(actsInRange) < 100:
                    return jsonify(actsInRange), 200
                else:
                    return jsonify({}), 413    
            else:
                return jsonify({}), 204
        else:
            return jsonify({}), 405

    elif request.method == 'GET':
        if db.checkcategorynameInDb(categoryname):
            acts = db.getActsForCategoryFromDb(categoryname)
            if len(acts):
                if len(acts) < 100:
                    return jsonify(acts), 200
                else:
                    return jsonify({}), 413    
            else:
                return jsonify({}), 204
        else:
             return jsonify({}), 204   
    else:
        return jsonify({}), 405

# API 8 : Get acts size for a given category
@app.route('/api/v1/categories/<categoryname>/acts/size',methods=['GET','PUT','POST','DELETE'])
def getActsForCategory(categoryname):
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == 'GET':
        if db.checkcategorynameInDb(categoryname):
            actsSize = db.getActsSizeForCategoryFromDb(categoryname)
            return jsonify(actsSize), 200
        else:
            return jsonify({}), 400        
    else:
        return jsonify({}),405

# API 9 : Upvote an act
@app.route('/api/v1/acts/upvote',methods=['POST','GET','PUT','DELETE'])
def upvoteAct():
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == 'POST':
        if len(request.get_json()) == 0:
            return jsonify({}),400
        actId = request.get_json()[0]
        if db.checkActInDb(actId):
            db.updateUpvoteForActInDb(actId)
            return jsonify({}),200
        else:
            return jsonify({}),400
    else:
        return jsonify({}),405

# API 10 : Remove an act
@app.route('/api/v1/acts/<actId>',methods = ['DELETE','GET','PUT','POST'])
def removeAct(actId):
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == 'DELETE':
        if db.checkActInDb(actId):
            db.deleteActFromDb(actId)
            return jsonify({}), 200  
        else:
            return jsonify({}), 400
    else:
        return jsonify({}), 405

# API 11 : Upload an act
@app.route('/api/v1/acts',methods = ['DELETE','GET','PUT','POST'])
def uploadAnAct():
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == "POST":
        actInJsonFormat = request.get_json()
        if len(actInJsonFormat) == 6:
            if service.validateActInputFormat(actInJsonFormat):
                if db.checkActInDb(actInJsonFormat["actId"]) == 0:
                    if service.checkUserName(actInJsonFormat["username"]) == 0:
                        if service.validateTimeFormat(actInJsonFormat["timestamp"]):
                            if db.checkcategorynameInDb(actInJsonFormat["categoryName"]) == 1:
                                if service.validateImageFormat(actInJsonFormat["imgB64"]):
                                    db.addActToDb(actInJsonFormat)
                                    return jsonify({}), 201
                                else:
                                    return jsonify({}), 400
                            else:
                                return jsonify({}), 400
                        else:
                            return jsonify({}), 400
                    else:
                        return jsonify({}), 400    
                else:
                    return jsonify({}), 400    
            else:
                return jsonify({}), 400    
        else:
            return jsonify({}), 400
    
    return jsonify({}), 405

# API for counting number of acts
@app.route("/api/v1/acts/counts", methods = ['POST','DELETE','GET','PUT'])
def actsCount():
    if checkServerCrashStatus():
        return jsonify(None), 500
    incrementHTTPRequestCount()
    if request.method == "GET":
        numberOfActs = db.getTotalNumberOfActs()
        return jsonify(numberOfActs), 200
    else:
        return jsonify({}), 400

# API for counting the number of HTTP request made
@app.route("/api/v1/_count", methods = ['POST','DELETE','GET','PUT'])
def requestsCounter():
    if checkServerCrashStatus():
        return jsonify(None), 500
    
    if request.method == "GET":
        return jsonify([getHTTPRequestCount()]), 200
    elif request.method == "DELETE":
        resetHTTPRequestCounter()
        return jsonify({}), 200
    else:
        return jsonify({}), 400    

# API for getting the health for the Server
@app.route("/api/v1/_health", methods = ["GET", "POST", "DELETE", "PUT"])
def getHealth():
    if checkServerCrashStatus():
        return jsonify(None), 500
    if request.method == "GET":
        columnName = "Health_Column"
        status = db.addTableToDb()
        if status == 1:
            status = db.deleteTableFromDb()
            if status == 1:
                return jsonify(None), 200
            else:
                return jsonify(None), 500
        else:
            return jsonify(None), 500
        return jsonify(None), 500
    else:
        return jsonify({}), 405

# API for crashing the server
@app.route("/api/v1/_crash", methods = ["GET", "POST", "DELETE", "PUT"])
def crashFunction():
    global crashCount
    if request.method == "POST":
        if crashCount == 0:
            crashCount += 1
            return jsonify(None), 200
        else:
            return jsonify(None), 500
    else:
        return jsonify({}), 405

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = '80',debug=True)