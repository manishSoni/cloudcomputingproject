# Function for validating password if the given password is in sha1 format or no
def validatePassword(password):
    if len(password) != 40:
        return False
    try:
        shaInt = int(password, 16)
    except ValueError:
        return False
    return True

# Function for validating user input format
def validateUserFormat(userDataInJsonFormat):
    try:
        userDataInJsonFormat["username"]
        try:
            userDataInJsonFormat["password"]
        except:
            return 0
    except:
        return 0
    return 1


