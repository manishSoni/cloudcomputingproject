from flask import Flask, jsonify, request
import DataBase as db
import ServiceLayer as service
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.config['JSON_SORT_KEYS'] = False
app.config["CORS_SUPPORTS_CREDENTIALS"]=True
requestsCount = 0

# API  : Add a user, list  all users
@app.route('/api/v1/users', methods = ['POST','DELETE','GET','PUT'])
def addUser():
    global requestsCount
    requestsCount += 1

    if request.method == 'POST': 
        try:
            userDataInJsonFormat = request.get_json()
            if service.validateUserFormat(userDataInJsonFormat):
                if db.checkUserNameInDb(userDataInJsonFormat['username']) :
                    if service.validatePassword(userDataInJsonFormat['password']):
                        userDataList = []
                        db.addUserDataToDb(userDataInJsonFormat)
                        db.getUserDataFromDb(userDataList)
                        return jsonify({}), 201
                    else:
                        return jsonify({}), 400
                else:
                    return jsonify({}), 400
            else:
                return jsonify({}), 400
        except:
            return jsonify({}), 400
    
    elif request.method == "GET":
        userNameList = db.getUserNameFromDb()
        if len(userNameList) != 0:
            return jsonify(userNameList), 200
        else:
            return jsonify({}), 204
    
    else:
        return jsonify({}),405    

# API 2 : Remove a user
@app.route('/api/v1/users/<username>', methods = ['POST','DELETE','GET','PUT'])
def deleteUser(username):
    global requestsCount
    requestsCount += 1

    if request.method == "DELETE":
        try:
            if db.checkUserNameInDb(username) == 0:
                db.deleteUserFromDb(username)
                return jsonify({}), 200
            else:
                return jsonify({}),400
        except:
            return jsonify({}), 400        
    else:
        return jsonify({}),405




@app.route("/api/v1/_count", methods = ['POST','DELETE','GET','PUT'])
def requestsCounter():
    global requestsCount
    if request,method == "GET":
        return jsonify([requestsCount]), 200
    elif request.method == "DELETE":
        requestsCount = 0
        return jsonify({}), 200
    else:
        return jsonify({}), 400



if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = '5000',debug=True)