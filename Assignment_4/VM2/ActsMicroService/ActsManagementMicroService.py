 #!flask/bin/python
from flask import Flask, jsonify, request
import hashlib
import time
import requests
import DataBase as db
import ServiceLayer as service
from flask_cors import CORS


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.config['JSON_SORT_KEYS'] = False
app.config["CORS_SUPPORTS_CREDENTIALS"]=True
requestsCount = 0



@app.route("/getCategoryListCallFromClient", methods = ["GET"])
def getCategoryListCallFromClient():
    categoryDictionary = {}
    db.getCategoryFromDb(categoryDictionary)
    print(categoryDictionary)
    categories = []
    for key, value in categoryDictionary.items():
        categories.append({key:value})
    return jsonify({'categories': categories}), 200
        
# API 3 and 4 : List all categories and Add a category
@app.route('/api/v1/categories', methods = ['POST','DELETE','GET','PUT'])
def categories():
    global requestsCount
    requestsCount += 1

    inputCategory = []
    try:
        inputCategory = (request.get_json())
    except:
        inputCategory = []
    if inputCategory != None and len(inputCategory):
        if request.method == 'POST': 
                if len(inputCategory):
                    print(db.checkcategorynameInDb(inputCategory[0]))
                    if db.checkcategorynameInDb(inputCategory[0]) == 0:
                        db.addCategoryToDb(inputCategory[0])
                        categoryList = {}
                        db.getCategoryFromDb(categoryList)
                        return jsonify({}), 201
                    else:
                        return jsonify({}), 400
                else:
                    return jsonify({}),400        
        else:
            return jsonify({}),405
    else:
        if request.method == 'GET': 
            categoryDictionary = {}
            db.getCategoryFromDb(categoryDictionary)
            if len(categoryDictionary):
                return jsonify(categoryDictionary), 200
            else:
                return jsonify({}),204        
        else:
            return jsonify({}),405
        return jsonify({}), 204 
            

# API 5 : Remove a category
@app.route('/api/v1/categories/<categoryname>', methods = ['POST','DELETE','GET','PUT'])
def deleteCategories(categoryname):
    global requestsCount
    requestsCount += 1

    if request.method == 'DELETE': 
            if db.checkcategorynameInDb(categoryname):
                db.deleteCategoryFromDb(categoryname)
                return jsonify({}), 200
            else:
                return jsonify({}),400        
    else:
        return jsonify({}),405
    
# API 6 and 7 : Get acts in a given range and get all the acts of a category    
@app.route('/api/v1/categories/<categoryname>/acts',methods=['GET','PUT','POST','DELETE'])
def getActsInCategory(categoryname):
    global requestsCount
    requestsCount += 1

    start = request.args.get('start')
    end = request.args.get('end')
    if (type(start) == int or type(end) == int) or (type(start) == str or type(end) == str):
        if (type(start) == str or type(end) == str):
            start = int(start)
            end = int(end)
        if start < 0 or end < 0 or start > end:
            return jsonify({}), 204
        response = requests.get("http://" + Host + ":" + port+ "    /api/v1/categories/" + categoryname + "/acts/size")   
        if start > len(response.json()):
            return jsonify({}), 400
        if request.method == 'GET':
            actsInRange = db.getActsInGivenRange(categoryname, start, end)
            if len(actsInRange):
                if len(actsInRange) < 100:
                    return jsonify(actsInRange), 200
                else:
                    return jsonify({}), 413    
            else:
                return jsonify({}), 204
        else:
            return jsonify({}), 405

    elif request.method == 'GET':
        if db.checkcategorynameInDb(categoryname):
            acts = db.getActsForCategoryFromDb(categoryname)
            if len(acts):
                if len(acts) < 100:
                    return jsonify(acts), 200
                else:
                    return jsonify({}), 413    
            else:
                return jsonify({}), 204
        else:
             return jsonify({}), 204   
    else:
        return jsonify({}), 405

# API 8 : Get acts size for a given category
@app.route('/api/v1/categories/<categoryname>/acts/size',methods=['GET','PUT','POST','DELETE'])
def getActsForCategory(categoryname):
    global requestsCount
    requestsCount += 1

    if request.method == 'GET':
        if db.checkcategorynameInDb(categoryname):
            actsSize = db.getActsSizeForCategoryFromDb(categoryname)
            print(actsSize)
            return jsonify(actsSize), 200
        else:
            return jsonify({}), 400        
    else:
        return jsonify({}),405

# API 9 : Upvote an act
@app.route('/api/v1/acts/upvote',methods=['POST','GET','PUT','DELETE'])
def upvoteAct():
    global requestsCount
    requestsCount += 1

    if request.method == 'POST':
        if len(request.get_json()) == 0:
            return jsonify({}),400
        actId = request.get_json()[0]
        if db.checkActInDb(actId):
            db.updateUpvoteForActInDb(actId)
            return jsonify({}),200
        else:
            return jsonify({}),400
    else:
        return jsonify({}),405

# API 10 : Remove an act
@app.route('/api/v1/acts/<actId>',methods = ['DELETE','GET','PUT','POST'])
def removeAct(actId):
    global requestsCount
    requestsCount += 1

    if request.method == 'DELETE':
        if db.checkActInDb(actId):
            db.deleteActFromDb(actId)
            return jsonify({}), 200  
        else:
            return jsonify({}), 400
    else:
        return jsonify({}), 405

# API 11 : Upload an act
@app.route('/api/v1/acts',methods = ['DELETE','GET','PUT','POST'])
def uploadAnAct():
    global requestsCount
    requestsCount += 1
    if request.method == "POST":
        actInJsonFormat = request.get_json()
        if len(actInJsonFormat) == 6:
            if service.validateActInputFormat(actInJsonFormat):
                if db.checkActInDb(actInJsonFormat["actId"]) == 0:
                    if service.checkUserName(actInJsonFormat["username"]) == 0:
                        if service.validateTimeFormat(actInJsonFormat["timestamp"]):
                            if db.checkcategorynameInDb(actInJsonFormat["categoryName"]) == 1:
                                if service.validateImageFormat(actInJsonFormat["imgB64"]):
                                    db.addActToDb(actInJsonFormat)
                                    return jsonify({}), 201
                                else:
                                    return jsonify({}), 400
                            else:
                                return jsonify({}), 400
                        else:
                            return jsonify({}), 400
                    else:
                        return jsonify({}), 400    
                else:
                    return jsonify({}), 400    
            else:
                return jsonify({}), 400    
        else:
            return jsonify({}), 400
    
    return jsonify({}), 405



@app.route("/api/v1/acts/counts", methods = ['POST','DELETE','GET','PUT'])
def actsCount():
    global requestsCount
    requestsCount += 1
    if request.method == "GET":
        numberOfActs = db.getTotalNumberOfActs()
        return jsonify(numberOfActs), 200
    else:
        return jsonify({}), 400



@app.route("/api/v1/_count", methods = ['POST','DELETE','GET','PUT'])
def requestsCounter():
    global requestsCount
    if request.method == "GET":
        return jsonify([requestsCount]), 200
    elif request.method == "DELETE":
        requestsCount = 0
        return jsonify({}), 200
    else:
        return jsonify({}), 400    

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = '5001',debug=True)