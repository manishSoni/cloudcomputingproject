import pandas as pd
import re

# Reading the file 
# File attributes : question, tags associated
def read_file(file_name):
    data_frame = pd.read_csv(file_name, nrows = 1000)
    tags = data_frame["tags"]
    questions = data_frame["title"]
    tags_list = []
    
    # Regex for getting only the tags in list format, given input is a string type , finally
    # appending the all the question tags list in tags_list
    # ["list"] -> "list"
    pattern = re.compile(r'\w+')
    for tag in tags:
        tags_list.append((pattern.findall(tag)))
    
    # Removing all those which hava a more than 5 tags associated with them
    for tag in tags_list:
        if len(tag) >= 5:            
            tags_list.remove(tag)
    
    data_frame = pd.DataFrame(tags_list)
    data_frame["questions"] = questions
    return data_frame